package matty.randommod.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.text.Text;

@Mixin(TitleScreen.class)
public class TitleMixin extends Screen {

	public TitleMixin(Text title) {
		super(title);
	}

	@Shadow
	private String splashText;
	
	@Inject(method = "init()V", at = @At("RETURN"), locals = LocalCapture.CAPTURE_FAILSOFT)
    private void initSplashText(CallbackInfo callbackInfo) {
            this.splashText = "Hello!";
    }

}
