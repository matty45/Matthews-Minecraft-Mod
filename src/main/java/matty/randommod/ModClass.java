package matty.randommod;

import matty.randommod.guns.Pistol;
import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

public class ModClass implements ModInitializer {
	// Add the items
	public static final Pistol PISTOL = new Pistol(new Item.Settings().group(ItemGroup.MISC));


	
	@Override
	public void onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.

		System.out.println("Matthew's Mod Loaded! Have a nice day!");
	}
}
